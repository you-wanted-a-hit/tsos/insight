import json
from kafka import KafkaConsumer
from decouple import config

class Consumer:
    def __init__(self, host, port, topics):
        self.consumer = KafkaConsumer(
            topics,
            bootstrap_servers=[f'{host}:{port}'],
            auto_offset_reset='earliest',
            enable_auto_commit=True,
            auto_commit_interval_ms=1000,
            group_id=config('KAFKA_CONSUMER_GROUP'), 
            value_deserializer=lambda msg: json.loads(msg.decode('utf-8')),
            key_deserializer=lambda key: key.decode('utf-8'))

    def get_consumer(self):
        return self.consumer
