# External dependencies
from decouple import config

# Built-in modules
import os.path
import base64
import json

#not relevant 
def validate_message(message):
    is_valid = False
    
    if message.key == 'Clustering':
        if message.value['query'] and message.value['type']:
            is_valid = message.value['type'] == 'json'    
    return is_valid


def get_config_json(file_path):
    path = os.path.abspath(f'../config/{file_path}')
    with open(path) as file:
        config_json = json.load(file)
        return config_json


