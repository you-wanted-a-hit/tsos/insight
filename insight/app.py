from decouple import config

from consumer.consumer import Consumer
from producer.producer import Producer
from manager import manager
from utils import utils
from log import log


import threading
import queue

task_queue = queue.Queue()

def consume_daemon():   
    logger = log.get_logger('CONSUMER')
    logger.info('Setting up consumer')

    # Setting up consumer
    host = config('KAFKA_HOST_NAME')
    port = config('KAFKA_HOST_PORT')

    consumer_topic = config('KAFKA_CONSUMER_TOPIC')
    consumer = Consumer(host=host, port=port, topics=consumer_topic).consumer
    
    logger.info('Completed setup stage')

    for message in consumer:
        logger.info(f'New message received {message.key} -> {message.value}')
        task_queue.put(message)
        logger.info(f'New message is in queue, waiting to be proccessed')

   

def run():
    logger = log.get_logger('MAIN')
    logger.info('Setting producer instance')
    host = config('KAFKA_HOST_NAME')
    port = config('KAFKA_HOST_PORT')
    producer_topic = config('KAFKA_PRODUCER_TOPIC')
    producer = Producer(host=host, port=port)

    # Launching consumer daemon
    threading.Thread(target=consume_daemon, daemon=True).start()

    while True:
        message = task_queue.get()
        logger.info('Requesting resources')
        logger.info(f'{message.key} -> {message.value}')
        if message.key=="train":
           results = manager.train(train_type=message.key, params=message.value)
           producer.publish(message.key, results, config("KAFKA_TRAIN_STATUS_TOPIC"))
        elif message.key=="predict": 
            predictions = manager.get_pred(pred_type=message.key, song_json=message.value)
            logger.info(f'Publishing requested prediction:{predictions}') 
            producer.publish(message.key, predictions, producer_topic) 
        