import json
from kafka import KafkaProducer
from decouple import config

# not needed currently
def serialize_value(msg):
    if msg['status'] == 200:
        msg['response'] = msg['response'].serialize()
    return msg

class Producer:
    def __init__(self, host, port):
        self.producer = KafkaProducer(
            bootstrap_servers=[f'{host}:{port}'],
            value_serializer= lambda msg: json.dumps(msg).encode('utf-8'),
            key_serializer= lambda key: key.encode('utf-8'))
        
    def publish(self, key, message, topic):
        self.producer.send(topic, message, key=key) #message = value 

