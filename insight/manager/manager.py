# Built-in modules
from datetime import datetime
import json
import os

# Modules
from models.classification import Classification
from models.time_series import TimeSeries
from models.clustering import Clustering

clustering = Clustering()
classification = Classification()
timeSeries = TimeSeries()

def train(train_type, params):
    if os.path.isfile(params['path']) and os.stat(params['path']).st_size > 0:
        try:
            status_code = timeSeries.retrain(params['path'])
        except Exception:
            status_code = 406
    else:
        status_code = 404

    return {'status': status_code}  

def get_pred(pred_type, song_json):
    res_dic = {
        'status' : 200,
        'request' : {
            'pred_type' : pred_type,
            'track' : song_json
        }
    }
    res_dic["clustering"] = clustering.predict(song_json['audio_features'])
    res_dic["classification"] = classification.predict(song_json['audio_features'])
    res_dic["timeSeries"] = timeSeries.predict(song_json['audio_features'])
    return res_dic

# classification:
# ['danceability','energy','loudness','speechiness','acousticness','instrumentalness','valence']
# clustering:
# ["danceability","energy","speechiness","acousticness","liveness","valence","tempo"]
# time series:
# ['danceability', 'energy', 'loudness','mode', 'speechiness', 'acousticness','liveness', 'valence', 'tempo']