from pyspark.sql import SparkSession
from pyspark.ml.clustering import KMeans
from pyspark.ml.clustering import BisectingKMeans
from pyspark.ml.evaluation import ClusteringEvaluator
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import StandardScaler
from pyspark.ml.linalg import Vectors


spark  = SparkSession.builder.appName('SongsClustering').getOrCreate()
df = spark.read.csv('./data/DF_Daily_Clustering.csv',inferSchema=True,header=True)
df.printSchema()


# In[3]:


df.show()


# ### We already saw that when we use the highest threshold we get good results, therefore, we will work only with songs that appeared at least 50 times in the top 50 spots.

# In[4]:


df = df.filter(df.Top_50_Appearnces > 50)


# In[5]:


df.count()


# In[6]:


df.show()


# In[7]:


df.columns


# ### Feature Selection For Better Performance

# #### We will select the features that worked the best in our clustering model. For more information about the feature selection, please take a look at the clustering notebook.

# In[8]:


feat_cols = ["danceability","energy","speechiness","acousticness","liveness","valence","tempo"]


# #### Setup the data to run on the model. Transform the data into a vector of features

# In[9]:


vec_assembler = VectorAssembler(inputCols=feat_cols, outputCol='features')


# #### Fixing data types

# In[10]:


df.dtypes


# In[11]:


df=df.withColumn('danceability',df['danceability'].cast("double").alias('danceability'))
df=df.withColumn('energy',df['energy'].cast("double").alias('energy'))


# In[12]:


df.dtypes


# In[13]:


final_data = vec_assembler.transform(df)


# ### We would like to find the optimal number of clusters based on silhouette score. For this task, we will run the algorithm with different number of clusters.

# In[14]:


evaluator = ClusteringEvaluator()


# In[15]:


for k in range(2,9):
    kmeans = KMeans(featuresCol = 'features',k=k)
    model = kmeans.fit(final_data)
    predictions = model.transform(final_data)
    silhouette = evaluator.evaluate(predictions)
    print("With K={}".format(k))
    print("Silhouette = "+str(silhouette))
    print('--'*20)
    


# ### We can see that with 4 clusters we get almost the same silhouette score as we get with 3 clusters. We would like to use the maximun number of clusters with the highest silhouette score and this is the reason we will go with 4 clusters.

# #### With the regular K-means algorithm, when the number of clusters was set to 4, we had a silhouette score of 0.615 while now we get a silhouette score of 0.77. This happens because of different implementation of the k-means - In pyspark we work with the ml.clustering library that uses k-means|| , while the sklearn.cluster library works with k-means++.

# ## Training and Predicting

# In[16]:


kmeans = KMeans(featuresCol = 'features',k=4)
model = kmeans.fit(final_data)
predictions = model.transform(final_data)
predictions = predictions.select('Track Name', 'Artist', 'prediction')


# ### Analysing the results

# In[17]:


predictions.where(predictions.prediction == 1).show()


# ### We can see that in cluster number 1 we got pop songs and latin songs

# In[18]:


predictions.where(predictions.prediction == 2).show()


# ### We can see that in cluster number 2 we got pop songs and hiphop songs

# In[19]:


predictions.where(predictions.prediction == 3).show()


# ### We can see that almost all the songs in cluster number 3 are pop songs

# In[20]:


predictions.where(predictions.prediction == 0).show()


# ### We can see that in cluster number 2 we got hiphop songs and pop songs

# The full analysing of the data was made in the clustering notebook.
