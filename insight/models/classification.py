import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, VotingClassifier, AdaBoostClassifier, BaggingRegressor
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.linear_model import LassoCV
from sklearn.datasets import make_regression
from sklearn.preprocessing import label_binarize
import xgboost as xgb
from sklearn.ensemble import VotingClassifier
import pickle
import json

from log import log
from time import sleep

from decouple import config


class Classification:
    __instance__ = None

    def __init__(self):
        self.df_train = None
        self.model = None
        self.df_before_norm = None
        self.genres_vectors = dict()
        self.genres_min_max = {
            "classical_max": list(),
            "classical_min": list(),
            "hiphop_max": list(),
            "hiphop_min": list(),
            "jazz_max": list(),
            "jazz_min": list(),
            "reggae_max": list(),
            "reggae_min": list(),
            "rock_max": list(),
            "rock_min": list()
        }        
        self.features = ['danceability', 'energy', 'loudness', 'speechiness', 'acousticness','instrumentalness', 'valence']
        self.features_description = {
            'danceability_max':"Your track is suitable for dancing based on a combination of musical elements including tempo, rhythm stability, beat strength, and overall regularity.",
            'danceability_min':"Your track is not suitable for dancing based on a combination of musical elements including tempo, rhythm stability, beat strength, and overall regularity.",
            'energy_max':"Your track is characterized by intensity and activity. it feels fast, loud and noisy.",
            'energy_min':"Your track is not characterized by intensity and activity. it does not feel fast or loud.",
            'loudness_max':"Your track is loud",
            'loudness_min':"Your track is not loud",
            'speechiness_max':"Your track is mostly speech-like and mostly made of spoken words.",
            'speechiness_min':"Your track is non-speech-like and mostly made of music.",
            'acousticness_max':"Your track is significantly acoustic",
            'acousticness_min':"Your track is not acoustic",
            'instrumentalness_max':"Your track contains no vocals, it is mostly composed of musical instruments sounds.",
            'instrumentalness_min':"Your track contains vocals, it is not mostly composed of musical instruments sounds.",
            'valence_max':"Your track sounds positive, it is happy, cheerful and euphoric",
            'valence_min':"Your track sounds negative, it is sad, depressed and angry."
        }
        self.insights_genres = {
            "classical": list(),
            "hiphop": list(),
            "jazz": list(),
            "reggae": list(),
            "rock": list()
        }
        # self.logger = log.get_logger('CLASS')
        self.train()

        if Classification.__instance__ is None:
           Classification.__instance__ = self
        else:
           raise Exception("You cannot create another Classification class")

    @staticmethod
    def get_instance():
        """ Static method to fetch the current instance."""
        if not Classification.__instance__:
            Classification()
        return Classification.__instance__

    def classificationSetup(self):
        # For this task we needed labeled songs by genre.
        # This information wasn't available in the top daily songs, so we collected the data from playlists of certain genre.
        # For example - 
        # We looked for Rock playlist -> Collected the songs from there -> Added the available features 
        # We decided to focus on 5 different genres that we think would have different characteristics. We chose Classical music, Hiphop and Rap, Jazz, Reggae and Rock. 
        
        # Import the data and merge each table to one DataFrame
        root_path = config('WORKDIR_PATH')
        # root_path = "C:/Users/ozsop/OneDrive/Desktop/Projects/DS/insight"
        df_classical = pd.read_csv(f"{root_path}/data/classical.csv")
        df_hiphop = pd.read_csv(f"{root_path}/data/hiphop.csv")
        df_jazz = pd.read_csv(f"{root_path}/data/jazz.csv")
        df_reggae = pd.read_csv(f"{root_path}/data/reggae.csv")
        df_rock = pd.read_csv(f"{root_path}/data/rock.csv")
        df = pd.concat([df_classical,df_hiphop,df_jazz,df_reggae,df_rock]) 

        # Shuffle the data
        df = df.sample(frac=1).reset_index(drop=True)

        # Remove duplicates- Some songs might appear in two tables, so we wanted to filter duplicates out.
        df = df.drop_duplicates(subset=['spotify_id'])
        self.df_before_norm = df[['danceability','energy','loudness','speechiness','acousticness','instrumentalness','valence','genre']]

        # Normalization of data
        for col in self.features:
            col_min = df[col].min()
            col_max = df[col].max()
            df[col] = ((df[col] - col_min) / (col_max - col_min))

        # Finding outliers
        relevantCol = ['energy','danceability','loudness','duration_ms','tempo','speechiness','acousticness','instrumentalness','liveness','valence']
        # In our data we marked songs as outliers if these songs have more than one features that is marked as an outlier.
        df.reset_index(drop=True)
        all_outliers = np.array([],dtype='int64')
        for feature in relevantCol:
            Q1 = np.percentile(df[feature],25)
            Q3 = np.percentile(df[feature],75)
            step = 1.5*(Q3-Q1)
            outlier_pts = df[ ~((df[feature]>=Q1-step) & (df[feature]<=Q3+step))]
            all_outliers = np.append(all_outliers,outlier_pts.index.values.astype('int64'))

        all_outliers2,indices = np.unique(all_outliers,return_inverse=True)
        counts = np.bincount(indices)
        outliers = all_outliers2[counts>1]
        indexes_to_keep = set(range(df.shape[0])) - set(outliers)
        filtered_data = df.take(list(indexes_to_keep))
        filtered_data.reset_index(drop=True)
        filtered_data.drop(columns=["spotify_id","name","artist","mode","liveness","tempo","duration_ms","time_signature","key"], inplace=True)

        self.df_train = filtered_data
        genres_vectors_df = self.df_train.groupby(['genre']).mean()
        self.genres_vectors = genres_vectors_df.to_dict('index')

        for index, row in genres_vectors_df.iterrows():
            for col in self.features:
                Q3 = np.percentile(genres_vectors_df[col],75)
                Q1 = np.percentile(genres_vectors_df[col],25)
                
                if row[self.features.index(col)] >= Q3:
                    self.genres_min_max[str(index)+"_max"].append(col)
                if row[self.features.index(col)] < Q1:
                    self.genres_min_max[str(index)+"_min"].append(col)
        
        for genre_val, features in self.genres_min_max.items():
            genre, t = genre_val.split("_")
            if len(self.genres_min_max[genre+"_min"])==0 and len(self.genres_min_max[genre+"_max"])==0:
                row  = genres_vectors_df.iloc[int(genre)]
                for col in self.features:
                    Q2 = np.percentile(genres_vectors_df[col],50)
                    if row[col] >= Q2:
                        self.genres_min_max[str(index)+"_max"].append(col)
                    if row[col] < Q2:
                        self.genres_min_max[str(index)+"_min"].append(col)

      
        for genre_val, features in self.genres_min_max.items():
            genre, t = genre_val.split("_")
            if t == "min":
                for feature in features:
                    self.insights_genres[genre].append(self.features_description[feature+"_min"])
            elif t == "max":
                for feature in features:
                    self.insights_genres[genre].append(self.features_description[feature+"_max"])
        
        # self.logger.info("Setup Done")


    def train(self):
        self.classificationSetup()
        df = self.df_train 

        # Drop columns that are not relevant for the learning process 
        X_train = df.drop("genre",1)
        Y_train = df["genre"]

        # KNN
        knn = KNeighborsClassifier(n_neighbors=11)
        knn.fit(X_train, Y_train)
        
        # Random Forest
        forest = RandomForestClassifier(n_estimators=260, max_depth = 11)
        forest.fit(X_train, Y_train)

        # Decision Tree
        tree = DecisionTreeClassifier(max_depth=6,criterion="entropy")
        tree.fit(X_train, Y_train)

        # Naïve Bayes
        nb = GaussianNB()
        nb.fit(X_train, Y_train)

        # XGBoost
        gbm = xgb.XGBClassifier( 
                                n_estimators=200,
                                max_depth=5,
                                objective='multi:softmax', 
                                num_class=5,
                                learning_rate=.1, 
                                gamma = 0.2
                            )

        fit_model = gbm.fit( 
                            X_train, Y_train, 
                            verbose=False
                        )

        # Ensemble method applying
        # We will combine all algorithms to one ensembled model that will take to consideration each of the algorithms and classify the data set.
        Ensemble = VotingClassifier(estimators=[('KNN', knn), ('RF', forest), ('DT', tree), ('NB', nb)],
                                    voting='soft',
                                    weights=[35, 45, 15, 5])

        fit_Ensemble=Ensemble.fit(X_train,Y_train)
        self.model = fit_Ensemble
        return "Train Classification is done"
        # self.logger.info("Train Done")


    def predict(self,song):       
        model = self.model

        df = self.df_before_norm
        song_features = [song['danceability'],song['energy'],song['loudness'],song['speechiness'],song['acousticness'],song['instrumentalness'],song['valence']]

        #Normalization of the song
        i=0
        for col in self.features:
            col_min = df[col].min()
            col_max = df[col].max()
            song_features[i] = (song_features[i] - col_min) / (col_max - col_min)
            i+=1
        
        genre_pred = model.predict([song_features])
        # songs = pd.read_csv(f"{config('WORKDIR_PATH')}/data/" + genre_pred[0] + ".csv").sample(n=10)

        # adding new insights 
        moreThan,lessThan,equals = self.assesments(songFeatures=song_features,vector=self.genres_vectors[genre_pred[0]])
        insightsDict = dict()
        insightsDict["danceability"] = ("Your track has higher danceability than the average of the genre which means it is more suitable for dancing based on a combination of musical elements including tempo, rhythm stability, beat strength, and overall regularity compared to the genre."
        ,"Your track has lower danceability than the average of the genre which means it is less suitable for dancing based on a combination of musical elements including tempo, rhythm stability, beat strength, and overall regularity compared to the genre.")
        insightsDict["energy"] = ("Your track has higher energy than the average of the genre - the track feels faster, louder and noisier in comparison to the genre."
        ,"Your track has lower energy than the average of the genre - the track feels slower and more quiet in comparison to the genre.")
        insightsDict["loudness"] = ("Your track is louder in comparison to the genre."
        ,"Your track is less loud in comparison to the genre.")
        insightsDict["speechiness"] = ("Your track has more spoken words compared to the rest of the genre."
                ,"Your track has less spoken words compared to the rest of the genre.")
        insightsDict["acousticness"] = ("Your track has more acoustic features in it than the average of the genre."
                ,"Your track has less acoustic features in it than the average of the genre.")
        insightsDict["instrumentalness"] = ("Compared to the average of the genre, your track contains less vocals, and mostly composed of musical instruments sounds."
                ,"Compared to the average of the genre, your track contains more vocals, and contain less musical instruments sounds.")
        insightsDict["valence"] = ("Your track sounds more positive, happier, cheerful and euphoric when comparing it to the rest of the genre.",
            "Your track sounds more negative, sad, depressed and angrywhen comparing it to the rest of the genre.")

        total_insights = dict()

        for i in range(len(moreThan)):
            total_insights[moreThan[i]] = insightsDict[moreThan[i]][0]
        for i in range(len(lessThan)):
            total_insights[lessThan[i]] = insightsDict[lessThan[i]][1]

        #end of adding
        
        dic = {}
        dic["genre_pred"] = genre_pred[0]
        dic["genre_vector"] = self.genres_vectors[genre_pred[0]]
        dic["classification_insights"] = total_insights #self.insights_genres[genre_pred[0]]
        # logger.info(x)
        return dic

    def assesments(self,songFeatures,vector):
        features = ['danceability','energy','loudness','speechiness','acousticness','instrumentalness','valence']
        lessThan = []
        moreThan = []
        equals = []
        for i in range(len(songFeatures)):
            if songFeatures[i] > vector[features[i]]:
                moreThan.append(features[i])
            elif songFeatures[i] < vector[features[i]]:
                lessThan.append(features[i])
            else:
                equals.append(features[i])
        return moreThan,lessThan,equals


# #Test
# classification = Classification() 

# starboy = {'danceability':0.681,'energy':0.594,'loudness':-7.028,'speechiness':0.282,
# 'acousticness':0.165,'instrumentalness':0.00000349,'valence':0.535
    
# }

# x = {'danceability':0.776,'energy':0.509,'loudness':-6.266,'speechiness':0.467,
# 'acousticness':0.192,'instrumentalness':0.00000149,'valence':0.485
# }
# song = json.dumps(starboy)
# song = json.loads(song)
# print(classification.predict(song))


