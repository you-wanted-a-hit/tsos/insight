import pandas as pd
import numpy as np
from numpy.linalg import norm
from datetime import datetime
from pandas import Series
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score
from sklearn import preprocessing
from itertools import permutations
import pickle
import json

from log import log
from decouple import config

class Clustering:
    __instance__ = None

    def __init__(self):
        # self.logger = log.get_logger('CLUSTER')
        self.df_train = None
        self.df_all_columns = None
        self.model = None
        self.clusters_vectors = dict()
        self.clusters_min_max = {
            "0_max": list(),
            "0_min": list(),
            "1_max": list(),
            "1_min": list(),
            "2_max": list(),
            "2_min": list(),
            "3_max": list(),
            "3_min": list()
        }        
        self.features = ["danceability","energy","speechiness","acousticness","liveness","valence","tempo"]
        self.features_description = {
            'danceability_max':"Your track is suitable for dancing based on a combination of musical elements including tempo, rhythm stability, beat strength, and overall regularity.",
            'danceability_min':"Your track is not suitable for dancing based on a combination of musical elements including tempo, rhythm stability, beat strength, and overall regularity.",
            'energy_max':"Your track is characterized by intensity and activity. it feels fast, loud and noisy.",
            'energy_min':"Your track is not characterized by intensity and activity. it does not feel fast or loud.",
            'speechiness_max':"Your track is mostly speech-like and mostly made of spoken words.",
            'speechiness_min':"Your track is non-speech-like and mostly made of music.",
            'acousticness_max':"Your track is significantly acoustic",
            'acousticness_min':"Your track is not acoustic",
            'liveness_max':"Your track is live, it is detected as one with presence of an audience.",
            'liveness_min':"Your track is not live",
            'valence_max':"Your track sounds positive, it is happy, cheerful and euphoric",
            'valence_min':"Your track sounds negative, it is sad, depressed and angry.",
            'tempo_max':"Your track is characterized by a high tempo (speed), it is derives directly from the average beat duration.",
            'tempo_min':"Your track is characterized by a low tempo (speed), it is derives directly from the average beat duration."
        }
        self.insights_clusters = {
            "0": list(),
            "1": list(),
            "2": list(),
            "3": list()
        }

        self.train()

        if Clustering.__instance__ is None:
           Clustering.__instance__ = self
        else:
           raise Exception("You cannot create another Clustering class")

    @staticmethod
    def get_instance():
        """ Static method to fetch the current instance."""
        if not Clustering.__instance__:
            Clustering()
        return Clustering.__instance__


    def clusteringSetup(self):
        # For our clustering model we decided to take the daily top 200 most streamed songs on spotify. The dataset contain the daily top 200 songs of each day from January 2017 until February 2021
        root_path = config('WORKDIR_PATH')
        # root_path = "C:/Users/adiza/OneDrive/Computer Science/Projects/DS Final Project/insight"
        df = pd.read_csv(f"{root_path}/data/top-200-daily.csv")

        # Removing unnecessary column
        df.drop(columns=["Unnamed: 0"], inplace=True)

        # We noticed that some dates are missing and filled with blanks. We will remove nulls, reset the indexes and create a backup copy of the data
        df.dropna(subset=['spotify_id'],inplace=True)#800 rows
        df.reset_index(drop=True)
        originalDf = df.copy()

        # Because our data set is based on a daily list, many songs appear nomerous of times.
        # For the clustring model we only need one representation of each song in the data frame. We will remove duplicates based on Spotify ID
        df = df.drop_duplicates(subset=['spotify_id'])

        # Checking if we can convert all dates to datetime format
        count = 0
        for time in df["date"]:
            try:
                pd.to_datetime(time, format='%m/%d/%Y')
            except ValueError:
                self.logger.info(f"When trying convert to datetime - Incorrect data format {time}")
                count = count + 1

        df['date'] = pd.to_datetime(df['date'], format='%m/%d/%Y')

        # Our final project is based on the assumption that successful songs have similarities in their features. We decided that we want to extract the truly successful songs. We wanted to add another feature that will help us measure the song success rate.
        # We built a function that checks how many times a song reached the top 50 spots.
        df["Top_50_Appearnces"] = self.top50Times(df,originalDf)

        # Finding outliers
        # In our data we marked songs as outliers if these songs have more than one features that is marked as an outlier.
        df.reset_index(drop=True)
        relevantCol = ['energy','danceability','loudness','tempo','speechiness','acousticness','instrumentalness','liveness','valence']
        all_outliers = np.array([],dtype='int64')
        for feature in relevantCol:
            Q1 = np.percentile(df[feature],25)
            Q3 = np.percentile(df[feature],75)
            step = 1.5*(Q3-Q1)
            outlier_pts = df[ ~((df[feature]>=Q1-step) & (df[feature]<=Q3+step))]
            all_outliers = np.append(all_outliers,outlier_pts.index.values.astype('int64'))

        all_outliers2,indices = np.unique(all_outliers,return_inverse=True)
        counts = np.bincount(indices)
        outliers = all_outliers2[counts>1]
        indexes_to_keep = set(range(df.shape[0])) - set(outliers)
        filtered_data = df.take(list(indexes_to_keep))
        filtered_data.reset_index(drop=True)
        
        filtered_data = filtered_data.loc[filtered_data["Top_50_Appearnces"] > 50]
        cols = ['spotify_id',"Position","Artist","Track Name",'Streams',"instrumentalness","danceability","energy","loudness","speechiness","acousticness","liveness","valence","tempo"]
        self.df_all_columns = filtered_data[cols]
        # Feature Selection For Better Performance
        self.df_train = filtered_data[self.features]
        # self.logger.info("Setup Done")

    def top50Times(self,uniqueDf,originalDf):
        Top50TimeArr = []
        originalDf = originalDf.loc[originalDf["Position"] <= 50]
        for _, row in uniqueDf.iterrows():
            spotifyID = row['spotify_id']
            tempDf = originalDf.loc[originalDf["spotify_id"] == spotifyID]
            index = tempDf.index
            numberOfAppearences = len(index)
            Top50TimeArr.append(numberOfAppearences)
        return Top50TimeArr
    
    def train(self):
        self.clusteringSetup()
        df_train = self.df_train
        df = self.df_all_columns
        kmeans_model = KMeans(n_clusters=4, random_state=10).fit(df_train)
        
        # Adding the predictions to the dataframe
        preds = kmeans_model.predict(df_train)
        df['cluster'] = preds
        df_train['cluster'] = preds

        clusters_vectors_df = df_train.groupby(['cluster']).mean()
        self.clusters_vectors = clusters_vectors_df.to_dict('index')
        
        for index, row in clusters_vectors_df.iterrows():
            for col in self.features:
                Q3 = np.percentile(clusters_vectors_df[col],75)
                Q1 = np.percentile(clusters_vectors_df[col],25)
                
                if row[self.features.index(col)] >= Q3:
                    self.clusters_min_max[str(index)+"_max"].append(col)
                if row[self.features.index(col)] < Q1:
                    self.clusters_min_max[str(index)+"_min"].append(col)
        
        for cluster_val, features in self.clusters_min_max.items():
            cluster, t = cluster_val.split("_")
            if len(self.clusters_min_max[cluster+"_min"])==0 and len(self.clusters_min_max[cluster+"_max"])==0:
                row  = clusters_vectors_df.iloc[int(cluster)]
                for col in self.features:
                    Q2 = np.percentile(clusters_vectors_df[col],50)
                    if row[col] >= Q2:
                        self.clusters_min_max[str(index)+"_max"].append(col)
                    if row[col] < Q2:
                        self.clusters_min_max[str(index)+"_min"].append(col)
        
        for cluster_val, features in self.clusters_min_max.items():
            cluster, t = cluster_val.split("_")
            if t == "min":
                for feature in features:
                    self.insights_clusters[cluster].append(self.features_description[feature+"_min"])
            elif t == "max":
                for feature in features:
                    self.insights_clusters[cluster].append(self.features_description[feature+"_max"])
        
        self.model = kmeans_model
        return "Train Clustering is done"
        # self.logger.info("Train Done")

    def predict(self,song):   
        model = self.model
        song_features = [song["danceability"],song["energy"],song["speechiness"],song["acousticness"],song["liveness"],song["valence"],song["tempo"]]
        cluster_pred = model.predict([song_features])
        dic = {}
        dic["cluster_pred"] = str(cluster_pred[0])
        dic["cluster_vector"] = self.clusters_vectors[cluster_pred[0]]
        dic["clustering_insights"] = self.insights_clusters[str(cluster_pred[0])]
        # logger.info(x)
        return dic

    def predictionsForTS(self,df):   
        cluster_df = df[["danceability","energy","speechiness","acousticness","liveness","valence","tempo"]]
        cluster = self.model.predict(cluster_df.to_numpy())
        df["Cluster"] = cluster
        return df


# #Test
# clustering = Clustering()
# x = {"danceability":0.599,"energy":0.448,"speechiness":0.0232,"acousticness":0.163,
# "liveness":0.106,"valence":0.168,"tempo":95.05
# }
# starboy = {"danceability":0.681,"energy":0.594,"speechiness":0.282,"acousticness":0.165,
# "liveness":0.134,"valence":0.535,"tempo":186.054
    
# }
# Say_You_Wont_Let_Go={"danceability":0.358,"energy":0.557,"speechiness":0.059,"acousticness":0.695,
# "liveness":0.0902,"valence":0.494,"tempo":85.043
# }

# Scars_To_Your_Beautiful={
#     "danceability":0.573,"energy":0.739,"speechiness":0.129,"acousticness":0.0285,
# "liveness":0.111,"valence":0.451,"tempo":97.085
# }
# song = json.dumps(starboy)
# song = json.loads(song)

# #song = [0.599,0.448,0.0232,0.163,0.106,0.168,95.05] # Ed Sheeran - Perfect 
# print(clustering.predict(song))

