import pandas as pd
import numpy as np
from datetime import datetime
from pandas import Series
from numpy.linalg import norm
from statsmodels.tsa.api import VAR
from statsmodels.tsa.arima.model import ARIMA
from tensorflow import keras
from tensorflow.keras import layers
import pickle
from scipy.spatial import distance
import json
import os

from log import log
from decouple import config

class TimeSeries:
    __instance__ = None

    def __init__(self):
        #self.logger = log.get_logger('TS')
        self.df_train = None
        self.df_before_norm = None 
        self.df_for_insights = None  
        self.df_forecast = None
        self.df_sucsess_songs = None
        self.df_failed_songs = None
        self.features = ['danceability', 'energy', 'loudness','mode', 'speechiness', 'acousticness','liveness', 'valence', 'tempo']

 
        self.months_dic = {"0": "next month",
                           "1": "in two months",
                           "2": "in three months"}

        self.var_model = None
        self.df_var_test_predictions = None
        self.df_var_forecasts = None
        
        self.arima_model = None
        self.df_arima_test_predictions = None
        self.df_arima_forecasts = None

        self.lstm_model = None
        self.df_lstm_test_predictions = None
        self.df_lstm_forecasts = None

        self.logger = log.get_logger('TS')

        self.train()

        if TimeSeries.__instance__ is None:
           TimeSeries.__instance__ = self
        else:
            raise Exception("You cannot create another TimeSeries class")

    @staticmethod
    def get_instance():
        """ Static method to fetch the current instance."""
        if not TimeSeries.__instance__:
            TimeSeries()
        return TimeSeries.__instance__

    def timeSeriesSetup(self, path):
        # We crossed information between two data sources.
        # We took lists of top 100 songs weekly from Bilboard and matched it with the features from spotify.
        # The data contains top 100 lists from 1990 until 2021.
        df = pd.read_csv(path)
        df.reset_index(drop=True)

        # Convert date column to datetime format
        count = 0
        for time in df["date"]:
            try:
                pd.to_datetime(time, format='%m/%d/%Y')
            
            except ValueError:
                self.logger.info(f"When trying convert to datetime - Incorrect data format {time}")
                count = count + 1
        if(count!=0):
            print(f"When trying convert to datetime - Incorrect data format {time}")
        df['date'] = pd.to_datetime(df['date'], format='%m/%d/%Y')


        # Add the month of the year for each week
        df['month_year'] = pd.to_datetime(df['date']).dt.to_period('M')
        self.df_for_insights = df.drop(columns=["title", "artist","spotify_id","is_new","last_pos","position","key","instrumentalness"])
        self.df_before_norm = df[self.features]
        # Normalizing the data - Making sure all features are between 0 and 1
        for col in self.features:
            col_min = df[col].min()
            col_max = df[col].max()
            df[col] = ((df[col] - col_min) / (col_max - col_min))
        
        # We will take all the best songs per months and aggregate them to find each month's mean features
        # We dropped string values and 2 features key and instrumentalness. In the clustering and classification models, we noticed that these features won't contribute to the model,
        # so we will drop them before starting to work on the time series model.
        fixed_df = df.drop(columns=["title", "artist","spotify_id","is_new","last_pos","position","key","instrumentalness"])
        
        # We want to aggregate the top songs of each month. Our hypothesis is that there is a similarity in features in successful song so we would like to predict which features will
        # be used in successful songs in the future. We measure song success by its peak position and by the number of appearences the song made in different weeks.
        fixed_df = fixed_df.loc[fixed_df["peak_pos"] < 15]
        fixed_df = fixed_df.loc[fixed_df["weeks"] > 10].groupby('month_year').mean() 
        fixed_df = fixed_df.drop(columns=["peak_pos", "weeks"])
        # fixed_df = fixed_df.set_index("month_year")
        self.logger.info("Setup Done")
        self.df_train = fixed_df

    # Invert the transformation to get the real forecast
    def invert_transformation(self, test, df_forecast):
        """Revert back the differencing to get the forecast to original scale."""
        df_fc = df_forecast.copy()
        columns = test.columns
        for col in columns:        
            df_fc[str(col)+'_forecast'] = test[col].iloc[-1] + df_fc[str(col)+'_1d'].cumsum()
        return df_fc

    def rmse_func(self,forecast, actual):
        return np.mean((forecast - actual)**2)**.5
    
    def best_rmse(self,results, forecasts, actual):
        var_rmse = self.rmse_func(results[0], actual)
        lstm_rmse = self.rmse_func(results[1], actual)
        arima_rmse = self.rmse_func(results[2], actual)
        rmse_values = [var_rmse, lstm_rmse, arima_rmse]
        return forecasts[rmse_values.index(min(rmse_values))]
    
    # convert series to supervised learning
    def series_to_supervised(self,data, n_in=1, n_out=1, dropnan=True):
        n_vars = 1 if type(data) is list else data.shape[1]
        df = pd.DataFrame(data)
        cols, names = list(), list()
        # input sequence (t-n, ... t-1)
        for i in range(n_in, 0, -1):
            cols.append(df.shift(i))
            names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
        # forecast sequence (t, t+1, ... t+n)
        for i in range(0, n_out):
            cols.append(df.shift(-i))
            if i == 0:
                names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
            else:
                names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
        # put it all together
        agg = pd.concat(cols, axis=1)
        agg.columns = names
        # drop rows with NaN values
        if dropnan:
            agg.dropna(inplace=True)
        return agg

    def var_train_test(self): 
        df = self.df_train
        features = self.features
        nobs = 3
        lag_order = 3
        df_train_var, df_test_var = df[0:-nobs], df[-nobs:]

        # Make the Time Series Stationary
        df_differenced = df_train_var.diff().dropna()

        # The optimal lag is observed at a lag order of 3, so we will select it.
        model = VAR(df_differenced)
        model_fitted = model.fit(3)
                        
        # Input data for predicting
        forecast_input = df_differenced.values[-lag_order:]

        fc = model_fitted.forecast(y=forecast_input, steps=nobs)
        df_var_forecast = pd.DataFrame(fc, columns=df.columns + '_1d')
        df_var_results = self.invert_transformation(df, df_var_forecast)
        df_var_results = df_var_results.loc[:, [ 'danceability_forecast', 'energy_forecast', 'loudness_forecast',
        'mode_forecast', 'speechiness_forecast', 'acousticness_forecast',
        'liveness_forecast', 'valence_forecast', 'tempo_forecast']]
        df_var_results.columns = features
        self.df_var_test_predictions = df_var_results

        # forecast
        df_differenced_f = df.diff().dropna()
        model_f = VAR(df_differenced_f)     
        model_fitted_f = model_f.fit(3)
        self.var_model = model_fitted_f
                
        # Input data for predicting
        forecast_input_f = df_differenced_f.values[-lag_order:]

        fc_f = model_fitted_f.forecast(y=forecast_input_f, steps=nobs)
        df_var_forecast_f = pd.DataFrame(fc, columns=df.columns + '_1d')
        df_var_results_f = self.invert_transformation(df, df_var_forecast_f)
        df_var_results_f = df_var_results_f.loc[:, [ 'danceability_forecast', 'energy_forecast', 'loudness_forecast',
        'mode_forecast', 'speechiness_forecast', 'acousticness_forecast',
        'liveness_forecast', 'valence_forecast', 'tempo_forecast']]
        df_var_results_f.columns = features
        self.df_var_forecasts = df_var_results_f
        # print("VAR results:")
        # print(self.df_var_forecasts)

    def arima_train_test(self):
        df = self.df_train
        features = self.features
        df_arima_results = pd.DataFrame()
        for feature in features:    
            test_arima = df[feature]
            nobs = 3
            df_train_arima, df_test_arima = test_arima[0:-nobs], test_arima[-nobs:]
            history = [x for x in df_train_arima.values]
            predictions = list()
            # walk-forward validation
            for t in range(len(df_test_arima)):
                model = ARIMA(history, order=(3,1,0))
                model_fit = model.fit()
                output = model_fit.forecast()
                y_hat = output[0]
                predictions.append(y_hat)
                obs = df_test_arima[t]
                history.append(obs)
           
            # evaluate forecasts
            df_arima_results[feature] = predictions
        self.df_arima_test_predictions = df_arima_results

        # forecast
        df_arima_results_f = pd.DataFrame()
        for feature in features:    
            test_arima_f = df[feature]
            nobs = 3
            history_f = [x for x in test_arima_f.values]
            predictions_f = list()
            # walk-forward validation
            for t in range(3):
                model_f = ARIMA(history_f, order=(3,1,0))
                model_fit_f = model_f.fit()
                output_f = model_fit_f.forecast()
                y_hat_f = output_f[0]
                predictions_f.append(y_hat_f)
                history.append(y_hat_f)
           
            # evaluate forecasts
            df_arima_results_f[feature] = predictions
        self.df_arima_forecasts = df_arima_results_f
        # print("ARIMA results:")
        # print(self.df_arima_forecasts)
   
    def lstm_train_test(self):
        df = self.df_train
        features = self.features
        values = df.values
        n_months = 3
        n_features = 9

        # frame as supervised learning
        reframed = self.series_to_supervised(values, n_months, 1)
        
        # split into train and test sets
        values = reframed.values
        n_train_months = reframed.shape[0]-3
        df_train_lstm = values[:n_train_months, :]
        df_test_lstm = values[n_train_months:, :]
        
        # split into input and outputs
        n_obs = n_months * n_features
        train_X, train_y = df_train_lstm[:, :n_obs], df_train_lstm[:,-n_features:]
        test_X, test_y = df_test_lstm[:, :n_obs], df_test_lstm[:, -n_features:]
        
        # reshape input to be 3D [samples, timesteps, features]
        train_X = train_X.reshape((train_X.shape[0], n_months, n_features))
        test_X = test_X.reshape((test_X.shape[0], n_months, n_features))

        # design network
        model = keras.Sequential()
        model.add(layers.LSTM(50, input_shape=(train_X.shape[1], train_X.shape[2])))
        model.add(layers.Dense(9))
        model.compile(loss='mae', optimizer='adam')
        
        # fit network
        model.fit(train_X, train_y, epochs=50, batch_size=72, validation_data=(test_X, test_y), verbose=0, shuffle=False)
        self.lstm_model = model

        # make predictions
        yhat = model.predict(test_X)
        df_lstm_results = pd.DataFrame(yhat, columns=df.columns)
        self.df_lstm_test_predictions = df_lstm_results

        #forecasts
        # design network
        new_model = keras.Sequential()
        new_model.add(layers.LSTM(50, stateful=True, batch_input_shape=(1,train_X.shape[1], train_X.shape[2])))
        new_model.add(layers.Dense(9))
        new_model.compile(loss='mae', optimizer='adam')

        new_model.set_weights(model.get_weights())
        new_model.reset_states()

        df_lstm_results_f = pd.DataFrame()
        data = df.copy()

        for t in range(3):
            values = data.values
            reframed = self.series_to_supervised(values, n_months, 1)
            ref_values = reframed.values
            t = ref_values[-1:, -9:]
            t = t.reshape((1, 1, n_features))
            prediction = new_model.predict([t])
            p = pd.DataFrame(prediction, columns = data.columns )
            data = pd.concat([data, p])
            df_lstm_results_f = pd.concat([df_lstm_results_f, p])

        df_lstm_results_f.columns = features

        self.df_lstm_forecasts = df_lstm_results_f
        # print("LSTM results:")
        # print(self.df_lstm_forecasts)

    def train(self):
        # path = "C:/Users/adiza/OneDrive/Computer Science/Projects/DS Final Project/insight/data/TimeSeries.csv"
        path = f"{config('WORKDIR_PATH')}/data/TimeSeries.csv"
        self.timeSeriesSetup(path)

        df = self.df_train
        features = self.features

        nobs = 3
        df_test = df[-nobs:]


        #VAR Train + Test
        self.var_train_test()
       
        #ARIMA Train + Test
        self.arima_train_test()

        #LSTM Train + Test
        self.lstm_train_test()
        
        self.logger.info("Train Done")

        #Taking future forecasts by : Taking forecats of each feature from the optimal model for it 
        final_prediction_df = pd.DataFrame()
        for feature in features:
            values = [self.df_var_test_predictions[feature], self.df_lstm_test_predictions[feature], self.df_arima_test_predictions[feature]]
            forecasts = [self.df_var_forecasts[feature], self.df_lstm_forecasts[feature], self.df_arima_forecasts[feature]]
            final_prediction_df[feature] = self.best_rmse(values, forecasts, df_test[feature])

        self.df_forecast = final_prediction_df   
        return "Train Time Series is done"

    def retrain(self, path):
        extention_csv_path = path
        # original_csv_path = "C:/Users/adiza/OneDrive/Computer Science/Projects/DS Final Project/insight/data/TimeSeries.csv"
        original_csv_path = f"{config('WORKDIR_PATH')}/data/TimeSeries.csv"
        extention_df = pd.read_csv(path, index_col='date')
        original_df = pd.read_csv(original_csv_path, index_col='date')
        os.remove(original_csv_path)
        df_completed = pd.concat([original_df, extention_df])
        df_completed.to_csv(original_csv_path, index=True, encoding='utf-8-sig')

        self.timeSeriesSetup(original_csv_path)

        nobs = 3
        df_test = self.df_train[-nobs:]

        #VAR Train + Test
        self.var_train_test()
       
        #ARIMA Train + Test
        self.arima_train_test()

        #LSTM Train + Test
        self.lstm_train_test()
        
        self.logger.info("Train Done")

        #Taking future forecasts by : Taking forecats of each feature from the optimal model for it 
        final_prediction_df = pd.DataFrame()
        for feature in self.features:
            values = [self.df_var_test_predictions[feature], self.df_lstm_test_predictions[feature], self.df_arima_test_predictions[feature]]
            forecasts = [self.df_var_forecasts[feature], self.df_lstm_forecasts[feature], self.df_arima_forecasts[feature]]
            final_prediction_df[feature] = self.best_rmse(values, forecasts, df_test[feature])
        
        os.remove(extention_csv_path)
        self.df_forecast = final_prediction_df   
        return 200
        

    def predict(self,song):
        errors = []
        song_features = [song['danceability'],song['energy'],song['loudness'],song['mode'],song['speechiness'],song['acousticness'],song['liveness'],song['valence'],song['tempo']]
        self.logger.debug(f"Song Features:{song_features}")
        #normalization of the song:
        df = self.df_before_norm.copy()
        for col in self.features:
            col_min = df[col].min()
            col_max = df[col].max()
            song_features[self.features.index(col)] = ((song_features[self.features.index(col)] - col_min) / (col_max - col_min))
        df_predictions = self.df_forecast.copy()
        for feature in self.features:
            self.logger.info(f'Prediction Vector for {feature}: {df_predictions[feature]}')

        self.logger.debug(f"Song Features:{song_features}")


        # logger.info("df_predictions:\n", df_predictions)
        
        vectors = df_predictions.to_numpy()
        for vec in vectors:
            errors.append(norm(song_features-vec))

        insight_df = self.df_for_insights.copy()
        insight_df = insight_df.loc[insight_df["peak_pos"] < 25]
        insight_df = insight_df.loc[insight_df["weeks"] > 5].groupby('month_year').mean()
        insight_df = insight_df[self.features]

        # De-Normalization of the forecasts
        for col in self.features:
            col_min = df[col].min()
            col_max = df[col].max()
            df_predictions[col] = df_predictions[col] * (col_max - col_min) + col_min
            self.logger.info(f'Prediction Vector for {col}: {df_predictions[col].to_numpy().tolist()}')


        best_month = np.argmin(errors)
        dic = {}
        dic["errors"] = errors
        if np.amin(errors) > 0.7:
            dic["timeSeries_insights"] = "This song pobably won't succeed in the next 3 months."
        else:
            dic["timeSeries_insights"] = f"This song may be successful {self.months_dic[str(best_month)]}."
        dic["feature_vectors"] = dict()


        for feature in self.features:
            self.logger.info(f'Prediction Vector for {feature}: {df_predictions[feature].to_numpy().tolist()}')

            total = insight_df[feature][-25:].to_numpy().tolist() + df_predictions[feature].to_numpy().tolist()
            dic["feature_vectors"][feature] = {
                "actual": total
            }
            # dic["feature_vectors"][feature] = {
                
            #     "actual": insight_df[feature][-13:].to_numpy().tolist(),
            #     "predictions": df_predictions[feature].to_numpy().tolist()
            # }

        # logger.info(x)
        return dic



# #Test
# model = TimeSeries()

# #1990
# #0.8
# Everything = {'danceability':0.57, 'energy':0.692, 'loudness':-9.551,'mode':1,
#  'speechiness':0.0303, 'acousticness':0.584,'liveness':0.203, 'valence':0.693, 'tempo':163.972}
# #0.57
# Just_Like_Jesse_James = {'danceability':0.4, 'energy':0.563, 'loudness':-10.233,'mode':1,
#  'speechiness':0.0307, 'acousticness':0.17,'liveness':0.133, 'valence':0.442, 'tempo':138.006}

# #2021
# Monster={'danceability':0.652, 'energy':0.383, 'loudness':-7.076,'mode':0,
#  'speechiness':0.0516, 'acousticness':0.0676,'liveness':0.0828, 'valence':0.549, 'tempo':145.765
# }
# Dynamite = {'danceability':0.746, 'energy':0.765, 'loudness':-4.41,'mode':0,
#  'speechiness':0.0993, 'acousticness':0.0112,'liveness':0.0936, 'valence':0.737, 'tempo':114.044
# }
# Mr_Right_Now = {'danceability':0.647, 'energy':0.667, 'loudness':-5.563,'mode':1,
#  'speechiness':0.304, 'acousticness':0.231,'liveness':0.133, 'valence':0.704, 'tempo':172.08
# }
# Stand_and_deliver = {"danceability": 0.659,"energy": 0.578,"loudness": -8.204,"mode": 1,
# "speechiness": 0.029,"acousticness": 0.675,"liveness": 0.107,"valence": 0.512,"tempo": 109.874
# }
# astronaut ={"danceability": 0.778,"energy": 0.695,"loudness": -6.865,"mode": 0,
# "speechiness": 0.0913,"acousticness": 0.175,"liveness": 0.15,"valence": 0.472,"tempo": 149.996}
# yuval_hambulbal = {"danceability": 0.625,"energy": 0.506,"loudness": -9.554,"mode": 1,
#       "speechiness": 0.0466,"acousticness": 0.217,"liveness": 0.0794,"valence": 0.437,"tempo": 105.037}
# depth_metal_1 = {"danceability": 0.549,"energy": 0.992,"loudness": -3.712,"mode": 1,
#         "speechiness": 0.116,"acousticness": 0.797,"liveness": 0.196,"valence": 0.877,"tempo": 172.342}

# depth_metal_2 = {"danceability": 0.486,"energy": 0.932,"loudness": -6.745,"mode": 1,
#       "speechiness": 0.166,"acousticness": 0.0000681,"liveness": 0.318,"valence": 0.29,"tempo": 120.001}

# yuval = {'danceability': 0.205, 'energy': 0.912, 'loudness': -4.398, 'mode': 1,
#  'speechiness': 0.0516, 'acousticness': 0.0235, 'liveness': 0.394, 'valence': 0.235, 'tempo': 101.591}

# song = json.dumps(yuval)
# song = json.loads(song)
# prediction = model.predict(song)
# print(prediction)

# path_extention = 'C:/Users/adiza/OneDrive/Computer Science/Projects/DS Final Project/insight/data/extention_ts.csv'
# model.retrain(path_extention)
# prediction = model.predict(song)
# print(prediction)
